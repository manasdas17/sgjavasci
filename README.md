author  ierturk @ StarGateInc <ierturk@ieee.org>
version V0.0.0
date    06-Sep-2014
brief   Xcos XML Diagram Importer into Java Tree

COPYRIGHT 2014 StarGate Inc <http://www.stargate-tr.com>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
